import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { User } from '../../interfaces/user.interface'; // Import the User interface
import { SessionService } from 'src/app/services/session.service'; // Import the SessionService
import { UserService } from 'src/app/services/user.service'; // Import the UserService

import { MeComponent } from './me.component';
import { SessionInformation } from 'src/app/interfaces/sessionInformation.interface';

describe('MeComponent', () => {
  let component: MeComponent;
  let fixture: ComponentFixture<MeComponent>;
  let userService: UserService;
  let sessionService: SessionService;
  let matSnackBar: MatSnackBar;

  const mockUser: User = {
    id: 1,
    email: 'test@example.com',
    firstName: 'Test',
    lastName: 'User',
    admin: true,
    password: 'testpassword',
    createdAt: new Date(),
  };

  const sessionInfo: SessionInformation = {
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ5b2dhQHN0dWRpby5jb20iLCJpYXQiOjE3MTIxNDgzMzAsImV4cCI6MTcxMjIzNDczMH0.XsZaM_eWYH7bVYNP4hkAUQCJ3k3NHT2UKLSUnPi9ddYUwbfbRhuhwS5P7TabK2FcBd2PPQ4Z5VSDr07brD6-5Q",
    "type": "Bearer",
    "id": 1,
    "username": "yoga@studio.com",
    "firstName": "Admin",
    "lastName": "Admin",
    "admin": true
  };

  const mockUserService = {
    delete: jest.fn(() => of(null)),
    getById: jest.fn(() => of(mockUser)),
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MeComponent],
      imports: [
        MatSnackBarModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        RouterTestingModule,
      ],
      providers: [
        { provide: UserService, useValue: mockUserService },
        SessionService,
        MatSnackBar,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MeComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService); // Update to correct injection token
    sessionService = TestBed.inject(SessionService); // Update to correct injection token
    matSnackBar = TestBed.inject(MatSnackBar);

    // Set session information before ngOnInit is called
    sessionService.sessionInformation = sessionInfo;

    // Detect changes to trigger ngOnInit
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should delete user account and navigate to home', async () => {
    // Spy on userService.delete method
    const deleteSpy = jest.spyOn(userService, 'delete').mockReturnValue(of(null));

    // Spy on matSnackBar.open method
    const openSnackBarSpy = jest.spyOn(matSnackBar, 'open');

    // Spy on sessionService.logOut method
    const logOutSpy = jest.spyOn(sessionService, 'logOut');

    // Spy on router.navigate method
    const navigateSpy = jest.spyOn(component['router'], 'navigate');

    // Call delete method
    component.delete();

    await fixture.whenStable();

    // Expectations
    expect(deleteSpy).toHaveBeenCalled();
    expect(deleteSpy).toHaveBeenCalledWith("1");
    expect(openSnackBarSpy).toHaveBeenCalledWith(
      'Your account has been deleted !',
      'Close',
      { duration: 3000 }
    );
  });
});
