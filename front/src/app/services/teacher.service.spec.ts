import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { TeacherService } from './teacher.service';

describe('TeacherService', () => {
  let service: TeacherService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TeacherService]
    });
    service = TestBed.inject(TeacherService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return list of teachers', () => {
    const mockTeachers = [{ id: '1', name: 'Teacher 1' }, { id: '2', name: 'Teacher 2' }];
    service.all().subscribe(teachers => {
      expect(teachers).toEqual(mockTeachers);
    });

    const req = httpMock.expectOne('api/teacher');
    expect(req.request.method).toBe('GET');
    req.flush(mockTeachers);
  });

  it('should handle error when fetching list of teachers fails', () => {
    const errorMessage = 'Error fetching teachers';
    service.all().subscribe({
      error: err => {
        expect(err).toBe(errorMessage);
      }
    });

    const req = httpMock.expectOne('api/teacher');
    req.flush(errorMessage, { status: 404, statusText: 'Not Found' });
  });
});
