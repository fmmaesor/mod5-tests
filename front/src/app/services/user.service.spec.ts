import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { UserService } from './user.service';

describe('UserService', () => {
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get user by ID', () => {
    const mockUser = { id: '1', name: 'User 1' };
    const userId = '1';

    service.getById(userId).subscribe(user => {
      expect(user).toEqual(mockUser);
    });

    const req = httpMock.expectOne(`api/user/${userId}`);
    expect(req.request.method).toBe('GET');
    req.flush(mockUser);
  });

  it('should delete user by ID', () => {
    const userId = '1';

    service.delete(userId).subscribe(result => {
      expect(result).toBeTruthy(); // Assuming successful deletion returns a truthy value
    });

    const req = httpMock.expectOne(`api/user/${userId}`);
    expect(req.request.method).toBe('DELETE');
    req.flush({}); // Mock successful deletion response
  });

  it('should handle error when getting user by ID fails', () => {
    const userId = '1';
    const errorMessage = 'Error fetching user';

    service.getById(userId).subscribe({
      error: err => {
        expect(err).toBe(errorMessage);
      }
    });

    const req = httpMock.expectOne(`api/user/${userId}`);
    req.flush(errorMessage, { status: 404, statusText: 'Not Found' });
  });

  it('should handle error when deleting user by ID fails', () => {
    const userId = '1';
    const errorMessage = 'Error deleting user';

    service.delete(userId).subscribe({
      error: err => {
        expect(err).toBe(errorMessage);
      }
    });

    const req = httpMock.expectOne(`api/user/${userId}`);
    req.flush(errorMessage, { status: 500, statusText: 'Internal Server Error' });
  });
});
