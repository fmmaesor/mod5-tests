import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, throwError } from 'rxjs';
import { SessionInformation } from 'src/app/interfaces/sessionInformation.interface';
import { SessionService } from 'src/app/services/session.service';
import { LoginRequest } from '../../interfaces/loginRequest.interface';
import { AuthService } from '../../services/auth.service';

import { LoginComponent } from './login.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: AuthService;
  let router: Router;
  let sessionService: SessionService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        ReactiveFormsModule
      ],
      providers: [
        {
          provide: AuthService,
          useValue: {
            login: jest.fn()
          }
        },
        SessionService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    router = TestBed.inject(Router);
    sessionService = TestBed.inject(SessionService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call authService.login and navigate on successful login', () => {
    const loginRequest: LoginRequest = { email: 'test@example.com', password: 'password' };
    const sessionInfo: SessionInformation = {
      "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ5b2dhQHN0dWRpby5jb20iLCJpYXQiOjE3MTIxNDgzMzAsImV4cCI6MTcxMjIzNDczMH0.XsZaM_eWYH7bVYNP4hkAUQCJ3k3NHT2UKLSUnPi9ddYUwbfbRhuhwS5P7TabK2FcBd2PPQ4Z5VSDr07brD6-5Q",
      "type": "Bearer",
      "id": 1,
      "username": "yoga@studio.com",
      "firstName": "Admin",
      "lastName": "Admin",
      "admin": true
    };
    const authServiceSpy = jest.spyOn(authService, 'login').mockReturnValue(of(sessionInfo));
    const routerNavigateSpy = jest.spyOn(router, 'navigate').mockResolvedValue(true);

    component.form.patchValue(loginRequest);
    component.submit();

    expect(authServiceSpy).toHaveBeenCalledWith(loginRequest);
    expect(routerNavigateSpy).toHaveBeenCalledWith(['/sessions']);
    expect(component.onError).toBeFalsy();
  });

  it('should set onError to true on login error', () => {
    const loginRequest: LoginRequest = { email: 'test@example.com', password: 'password' };
    const authServiceSpy = jest.spyOn(authService, 'login').mockReturnValue(throwError('Login error'));

    component.form.patchValue(loginRequest);
    component.submit();

    expect(authServiceSpy).toHaveBeenCalledWith(loginRequest);
    expect(component.onError).toBeTruthy();
  });
});
