import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { RegisterRequest } from '../../interfaces/registerRequest.interface';
import { AuthService } from '../../services/auth.service';

import { RegisterComponent } from './register.component';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;
  let authService: AuthService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [RegisterComponent],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule,
        HttpClientModule,
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: AuthService,
          useValue: {
            register: jest.fn()
          }
        },
        {
          provide: Router, // Mock Router
          useValue: {
            navigate: jest.fn()
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call authService.register and set onError to false on successful registration', () => {
    const registerRequest: RegisterRequest = {
      email: 'test@example.com',
      firstName: 'John',
      lastName: 'Doe',
      password: 'password'
    };
    const authServiceSpy = jest.spyOn(authService, 'register').mockReturnValue(of());

    component.form.patchValue(registerRequest);
    component.submit();

    expect(authServiceSpy).toHaveBeenCalledWith(registerRequest);
    expect(component.onError).toBeFalsy();
  });

  it('should set onError to true on registration error', () => {
    const registerRequest: RegisterRequest = {
      email: 'test@example.com',
      firstName: 'John',
      lastName: 'Doe',
      password: 'password'
    };
    const authServiceSpy = jest.spyOn(authService, 'register').mockReturnValue(throwError('Registration error'));

    component.form.patchValue(registerRequest);
    component.submit();

    expect(authServiceSpy).toHaveBeenCalledWith(registerRequest);
    expect(component.onError).toBeTruthy();
  });
});
