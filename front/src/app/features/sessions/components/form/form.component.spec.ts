import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { expect } from '@jest/globals';
import { of } from 'rxjs';
import { SessionService } from '../../../../services/session.service';
import { Session } from '../../interfaces/session.interface';
import { SessionApiService } from '../../services/session-api.service';

import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { FormComponent } from './form.component';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  const mockSessionService = {
    sessionInformation: {
      admin: true
    }
  };

  const mockSessionApiService = {
    detail: jest.fn().mockReturnValue(of({} as Session)),
    create: jest.fn().mockReturnValue(of({} as Session)),
    update: jest.fn().mockReturnValue(of({} as Session))
  };

  const mockSnackBar = {
    open: jest.fn()
  };


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'sessions', component: FormComponent },
        ]),
        HttpClientModule,
        MatCardModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatSelectModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: SessionService, useValue: mockSessionService },
        { provide: SessionApiService, useValue: mockSessionApiService },
        { provide: MatSnackBar, useValue: mockSnackBar }
      ],
      declarations: [FormComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form for update', () => {
    const mockSession: Session = {
      name: 'Mock Session',
      date: new Date(),
      teacher_id: 1, // Corrected type to number
      description: 'Mock session description',
      users: []
    };
    jest.spyOn(TestBed.inject(SessionApiService), 'detail').mockReturnValue(of(mockSession));
    const spyInitForm = jest.spyOn(component as any, 'initForm');
    component.ngOnInit();

    expect(spyInitForm).toHaveBeenCalledWith();
  });

  it('should initialize form for creation', () => {
    jest.spyOn(TestBed.inject(SessionApiService), 'detail').mockReturnValue(of({} as Session)); // Corrected return type
    const spyInitForm = jest.spyOn(component as any, 'initForm');


    component.ngOnInit();

    expect(spyInitForm).toHaveBeenCalled();
  });

  it('should submit new session', () => {
    const mockSession: Session = {
      name: 'Mock Session',
      date: new Date(),
      teacher_id: 1, // Corrected type to number
      description: 'Mock session description',
      users: []
    };
    component.sessionForm = {
      value: mockSession
    } as any;
    const router = TestBed.inject(Router);
    const navigateSpy = jest
      .spyOn(router, 'navigate')
      .mockImplementation(() => Promise.resolve(true));

    component.submit();

    expect(mockSessionApiService.create).toHaveBeenCalledWith(mockSession);
    expect(navigateSpy).toHaveBeenCalledWith(['sessions']);
  });

  it('should submit updated session', () => {
    const mockSession: Session = {
      name: 'Mock Session',
      date: new Date(),
      teacher_id: 1, // Corrected type to number
      description: 'Mock session description',
      users: []
    };
    component.sessionForm = {
      value: mockSession
    } as any;
    component.onUpdate = true;
    component['id'] = 'session-id';
    const router = TestBed.inject(Router);
    const navigateSpy = jest
      .spyOn(router, 'navigate')
      .mockImplementation(() => Promise.resolve(true));

    component.submit();

    expect(mockSessionApiService.update).toHaveBeenCalledWith('session-id', mockSession);
    expect(navigateSpy).toHaveBeenCalledWith(['sessions']);
  });

  it('should exit page and show message', () => {
    const mockMessage = 'Mock message';
    jest.spyOn(mockSnackBar, 'open').mockImplementation();
    const router = TestBed.inject(Router);
    const navigateSpy = jest
      .spyOn(router, 'navigate')
      .mockImplementation(() => Promise.resolve(true));
    (component as any).exitPage(mockMessage);

    expect(mockSnackBar.open).toHaveBeenCalledWith(mockMessage, 'Close', { duration: 3000 });
    expect(navigateSpy).toHaveBeenCalledWith(['sessions']);
  });
});
