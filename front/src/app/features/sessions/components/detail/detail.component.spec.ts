import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { DetailComponent } from './detail.component';
import { SessionService } from '../../../../services/session.service';
import { TeacherService } from '../../../../services/teacher.service';
import { SessionApiService } from '../../services/session-api.service';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;
  let sessionService: SessionService;
  let sessionApiService: SessionApiService;
  let teacherService: TeacherService;
  let matSnackBar: MatSnackBar;
  let router: Router;

  const mockSession = {
    id: '1',
    teacher_id: '1',
    users: ['1']
  };

  const mockTeacher = {
    id: '1',
    name: 'Test Teacher'
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DetailComponent],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'sessions', component: DetailComponent },
        ]),
      ],
      providers: [
        { provide: ActivatedRoute, useValue: { snapshot: { paramMap: { get: () => '1' } } } },
        { provide: FormBuilder, useValue: {} },
        { provide: SessionService, useValue: { sessionInformation: { admin: true, id: 1 } } },
        { provide: SessionApiService, useValue: {
            detail: () => of(mockSession),
            delete: () => of({}),
            participate: () => of({}),
            unParticipate: () => of({})
          }
        },
        { provide: TeacherService, useValue: {
            detail: () => of(mockTeacher)
          }
        },
        { provide: MatSnackBar, useValue: { open: () => {} } },
        { provide: Router, useValue: {} }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    sessionService = TestBed.inject(SessionService);
    sessionApiService = TestBed.inject(SessionApiService);
    teacherService = TestBed.inject(TeacherService);
    matSnackBar = TestBed.inject(MatSnackBar);
    router = TestBed.inject(Router);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize session data on ngOnInit', () => {
    component.ngOnInit();
    expect(component.session).toEqual(mockSession);
    expect(component.isParticipate).toBeFalsy();
    expect(component.teacher).toEqual(mockTeacher);
  });


  it('should delete session and navigate to sessions', () => {
    const spyDelete = jest.spyOn(sessionApiService, 'delete').mockReturnValue(of({}));
    const spySnackBarOpen = jest.spyOn(matSnackBar, 'open');
    component.delete();
    expect(spyDelete).toHaveBeenCalledWith('1');
    expect(spySnackBarOpen).toHaveBeenCalledWith('Session deleted !', 'Close', { duration: 3000 });
  });

  it('should participate in session', () => {
    jest.spyOn(sessionApiService, 'participate').mockReturnValue(of());
    component.participate();
    expect(sessionApiService.participate).toHaveBeenCalledWith('1', '1');
  });

  it('should unparticipate in session', () => {
    jest.spyOn(sessionApiService, 'unParticipate').mockReturnValue(of());
    component.unParticipate();
    expect(sessionApiService.unParticipate).toHaveBeenCalledWith('1', '1');
  });
});
