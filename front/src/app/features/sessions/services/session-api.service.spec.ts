import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { SessionApiService } from './session-api.service';
import { expect } from '@jest/globals';
import { Session } from '../interfaces/session.interface';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';



describe('SessionsService', () => {
  let service: SessionApiService;
  let httpClient: HttpClient;

  const mockSession: Session = {
    id: 1,
    name: 'Session 1',
    description: 'Session description',
    date: new Date(),
    teacher_id: 1,
    users: []
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      imports:[
        HttpClientModule,
        RouterTestingModule,
      ],
      providers: [SessionApiService, HttpClient]
    });
    service = TestBed.inject(SessionApiService);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch all sessions', () => {

    jest.spyOn(httpClient, 'get').mockReturnValue(of(mockSession));
    service.all().subscribe(sessions => {
      expect(sessions).toEqual(mockSession);
    });
  });

  it('should fetch session detail', () => {
    jest.spyOn(httpClient, 'get').mockReturnValue(of(mockSession));
    const sessionId = '1';
    service.detail(sessionId).subscribe(session => {
      expect(session).toEqual(mockSession);
    });
  });

  it('should delete session', () => {
    const sessionId = '1';
    jest.spyOn(httpClient, 'delete').mockReturnValue(of({}));
    service.delete(sessionId).subscribe(response => {
      expect(response).toEqual({});
    });
  });

  it('should create session', () => {

    jest.spyOn(httpClient, 'post').mockReturnValue(of(mockSession));
    service.create(mockSession).subscribe(session => {
      expect(session).toEqual(mockSession);
    });
  });

  it('should update session', () => {
    const sessionId = '1';
    jest.spyOn(httpClient, 'put').mockReturnValue(of(mockSession));
    service.update(sessionId, mockSession).subscribe(session => {
      expect(session).toEqual(mockSession);
    });
  });

  it('should participate in session', () => {
    const sessionId = '1';
    const userId = '1';
    jest.spyOn(httpClient, 'post').mockReturnValue(of({}));
    service.participate(sessionId, userId).subscribe(response => {
      expect(response).toEqual({});
    });
  });

  it('should unparticipate in session', () => {
    const sessionId = '1';
    const userId = '1';
    jest.spyOn(httpClient, 'delete').mockReturnValue(of({}));
    service.unParticipate(sessionId, userId).subscribe(response => {
      expect(response).toEqual({});
    });
  });
});
