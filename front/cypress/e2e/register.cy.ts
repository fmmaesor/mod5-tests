describe('Account Creation', () => {
  it('should successfully create a new account', () => {
    // Visit the register page
    cy.visit('/register');

    cy.intercept('POST', '/api/auth/register', {
    })

    // Fill out the registration form
    cy.get('[formControlName=firstName]').type('John');
    cy.get('[formControlName=lastName]').type('Doe');
    cy.get('[formControlName=email]').type('john@example.com');
    cy.get('[formControlName=password]').type('password123');

    // Submit the form
    cy.get('button[type=submit]').click();

    // Ensure that user is redirected to the login page
    cy.url().should('include', '/login');
  });

  it('should disable the submit button for invalid registration input', () => {
    // Visit the register page
    cy.visit('/register');

    // Fill out the registration form with invalid data
    cy.get('[formControlName=firstName]').type('Jo'); // Too short
    cy.get('[formControlName=lastName]').type('Doe');
    cy.get('[formControlName=email]').type('invalid_email'); // Invalid email format
    cy.get('[formControlName=password]').type('pass'); // Too short

    // Ensure submit button is disabled
    cy.get('button[type=submit]').should('be.disabled');
  });
});
