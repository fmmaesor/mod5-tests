describe('Account Link', () => {
  beforeEach(() => {
    // Login before running the tests
    cy.visit('/login');

    cy.intercept('POST', '/api/auth/login', {
      body: {
        id: 1,
        username: 'userName',
        firstName: 'firstName',
        lastName: 'lastName',
        admin: true
      },
    })

    cy.intercept(
      {
        method: 'GET',
        url: '/api/session',
      },
      []).as('session')

    cy.get('input[formControlName=email]').type('yoga@studio.com');
    cy.get('input[formControlName=password]').type(`${'test!1234'}{enter}{enter}`
    );
  });

  it('should navigate to Account page when clicked', () => {

    cy.get('span[routerLink="me"].link').click();

    cy.url().should('include', '/me');
  });

  it('should allow to go back on the page', () => {

    cy.get('span[routerLink="me"].link').click();

    cy.get('mat-card-title').within(() => {
      // Check if the participate button is visible and click it
      cy.get('button').eq(0).click();
    });
  });

});
