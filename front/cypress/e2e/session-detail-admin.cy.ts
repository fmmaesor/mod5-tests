describe('Detail Component', () => {
  beforeEach(() => {
    // Login before running the tests
    cy.visit('/login')

    cy.intercept('POST', '/api/auth/login', {
      body: {
        id: 1,
        username: 'userName',
        firstName: 'firstName',
        lastName: 'lastName',
        admin: true
      },
    })

    cy.intercept(
      {
        method: 'GET',
        url: '/api/session',
      },
      [
        {
          id: 1,
          name: 'sessionName',
          description: 'sessionDescription',
          teacher_id: 1,
          users: [],
        }
      ]).as('session')

      cy.intercept(
        {
          method: 'GET',
          url: '/api/session/1',
        },
          {
            id: 1,
            name: 'sessionName',
            description: 'sessionDescription',
            teacher_id: 1,
            users: [],
          }
        ).as('session')

    cy.get('input[formControlName=email]').type("yoga@studio.com")
    cy.get('input[formControlName=password]').type(`${"test!1234"}{enter}{enter}`)

    cy.get('.items .item').should('have.length.gt', 0);
    // Find a session card and click the detail button
    cy.get('.items .item')
      .first()
      .within(() => {
        cy.get('mat-card-actions').within(() => {
          cy.get('button').eq(0).click();
        });
      });
  });

  it('should allow the user to delete in a session', () => {
    cy.get('mat-card-title').within(() => {
      // Check if the delete button is visible and click it
      cy.get('button').eq(1).click();
    });
  });

  it('should allow to go back on the page', () => {
    cy.get('mat-card-title').within(() => {
      // Check if the participate button is visible and click it
      cy.get('button').eq(0).click();
    });
  });

});
