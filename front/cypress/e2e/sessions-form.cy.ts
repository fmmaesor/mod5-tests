describe('Form Component', () => {
  beforeEach(() => {
    // Login before running the tests
    cy.visit('/login');

    cy.intercept('POST', '/api/auth/login', {
      body: {
        id: 1,
        username: 'userName',
        firstName: 'firstName',
        lastName: 'lastName',
        admin: true
      }
    }).as('login');

    cy.intercept('GET', '/api/session', [
      {
        id: 1,
        name: 'sessionName',
        description: 'sessionDescription',
        teacher_id: 1,
        date: '2023-01-01',
      }
    ]).as('sessions');

    cy.intercept('GET', '/api/session/1', {
      id: 1,
      name: 'sessionName',
      description: 'sessionDescription',
      teacher_id: 1,
      date: '2023-01-01',
    }).as('session');

    cy.intercept('GET', '/api/teachers', [
      {
        id: 1,
        firstName: 'John',
        lastName: 'Doe'
      }
    ]).as('teachers');

    cy.get('input[formControlName=email]').type("yoga@studio.com")
    cy.get('input[formControlName=password]').type(`${"test!1234"}{enter}{enter}`)
  });

  it('should navigate to create session form', () => {
    cy.visit('/sessions/create');
  });
});
