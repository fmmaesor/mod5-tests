package com.openclassrooms.starterjwt.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.starterjwt.controllers.SessionController;
import com.openclassrooms.starterjwt.dto.SessionDto;
import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.services.SessionService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SessionControllerItTest {

    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private ObjectMapper objectMapper;

    @InjectMocks
    private SessionController sessionController;

    @Test
    void testFindById() throws Exception {
        // Arrange
        long sessionId = 1L;
        Session session = new Session();
        // Mock the behavior to return null when getById is called

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/session/{id}", sessionId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testFindAll() throws Exception {
        // Arrange
        List<Session> sessions = new ArrayList<>();

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.get("/api/session")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void testCreate() throws Exception {
        // Arrange
        SessionDto sessionDto = new SessionDto();
        sessionDto.setName("Test Session");
        sessionDto.setDate(new Date());
        sessionDto.setTeacher_id(1L); // Assuming a valid teacher ID
        sessionDto.setDescription("Test Description");


        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/session")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(sessionDto)))
                .andExpect(status().isOk());
    }


    @Test
    void testUpdate() throws Exception {
        // Arrange
        long id = 1L;
        SessionDto sessionDto = new SessionDto();
        sessionDto.setName("Session Name");
        sessionDto.setDescription("Session Description");
        sessionDto.setTeacher_id(1L); // Provide a valid teacher_id
        sessionDto.setDate(new Date()); // Provide a valid date


        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.put("/api/session/{id}", id)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(sessionDto)))
                .andExpect(status().isOk());
    }


    @Test
    void testSave() throws Exception {
        // Arrange
        long id = 1L;


        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/session/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    void testParticipate() throws Exception {
        // Arrange
        long sessionId = 1L;
        long userId = 1L;

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.post("/api/session/{id}/participate/{userId}", sessionId, userId))
                .andExpect(status().isNotFound());
    }


    @Test
    void testNoLongerParticipate() throws Exception {
        // Arrange
        long sessionId = 1L;
        long userId = 1L;

        // Act & Assert
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/session/{id}/participate/{userId}", sessionId, userId))
                .andExpect(status().isNotFound());
    }
}
