package com.openclassrooms.starterjwt.ut.services;

import com.openclassrooms.starterjwt.models.Teacher;
import com.openclassrooms.starterjwt.services.TeacherService;
import com.openclassrooms.starterjwt.repository.TeacherRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TeacherServiceTest {

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherService teacherService;


    @Test
    void testFindAllTeachers() {
        // Create a list of teachers for testing
        List<Teacher> teachers = new ArrayList<>();
        teachers.add(new Teacher(1L, "Doe", "John", LocalDateTime.now(), LocalDateTime.now()));
        teachers.add(new Teacher(2L, "Smith", "Jane", LocalDateTime.now(), LocalDateTime.now()));

        // Mock the behavior of teacherRepository.findAll() to return the list of teachers
        when(teacherRepository.findAll()).thenReturn(teachers);

        // Call the findAll() method of the teacherService
        List<Teacher> foundTeachers = teacherService.findAll();

        // Verify that the returned list is equal to the list of teachers
        assertEquals(teachers, foundTeachers);
        // Verify that teacherRepository.findAll() was called once
        verify(teacherRepository, times(1)).findAll();
    }

    @Test
    void testFindTeacherById() {
        // Create a teacher object for testing
        Teacher teacher = new Teacher(1L, "Doe", "John", LocalDateTime.now(), LocalDateTime.now());

        // Mock the behavior of teacherRepository.findById() to return the teacher object
        when(teacherRepository.findById(1L)).thenReturn(Optional.of(teacher));

        // Call the findById() method of the teacherService
        Teacher foundTeacher = teacherService.findById(1L);

        // Verify that the returned teacher is equal to the expected teacher
        assertEquals(teacher, foundTeacher);
        // Verify that teacherRepository.findById() was called once with the correct ID
        verify(teacherRepository, times(1)).findById(1L);
    }
}
