package com.openclassrooms.starterjwt.ut.models;

import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.models.Teacher;
import com.openclassrooms.starterjwt.models.User;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SessionTest {

    @Test
    public void testSessionCreation() {
        // Create a Teacher
        Teacher teacher = new Teacher();
        teacher.setId(1L);
        teacher.setFirstName("John");
        teacher.setLastName("Doe");

        // Create a User
        User user1 = new User();
        user1.setId(1L);
        user1.setFirstName("Alice");
        user1.setLastName("Smith");
        user1.setEmail("alice@example.com");

        // Create another User
        User user2 = new User();
        user2.setId(2L);
        user2.setFirstName("Bob");
        user2.setLastName("Jones");
        user2.setEmail("bob@example.com");

        // Create a list of users
        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);

        // Create a Session
        Session session = new Session();
        session.setId(1L);
        session.setName("Test Session");
        session.setDate(new Date());
        session.setDescription("Test Description");
        session.setTeacher(teacher);
        session.setUsers(users);

        // Assert that the session is not null
        assertNotNull(session);

        // Assert that session attributes are set correctly
        assertEquals(1L, session.getId());
        assertEquals("Test Session", session.getName());
        assertEquals("Test Description", session.getDescription());
        assertEquals(teacher, session.getTeacher());
        assertEquals(users, session.getUsers());
    }
}
