package com.openclassrooms.starterjwt.ut.services;

import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.UserRepository;
import com.openclassrooms.starterjwt.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;


    @Test
    void testDeleteUser() {
        Long userId = 1L;

        // Call the delete() method of the userService
        userService.delete(userId);

        // Verify that userRepository.deleteById() was called once with the correct ID
        verify(userRepository, times(1)).deleteById(userId);
    }

    @Test
    void testFindUserById() {
        Long userId = 1L;
        User user = new User();
        user.setId(userId);

        // Mock the behavior of userRepository.findById() to return the user object
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        // Call the findById() method of the userService
        User foundUser = userService.findById(userId);

        // Verify that the returned user is equal to the expected user
        assertEquals(user, foundUser);
        // Verify that userRepository.findById() was called once with the correct ID
        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    void testFindUserById_UserNotFound() {
        Long userId = 1L;

        // Mock the behavior of userRepository.findById() to return an empty optional
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        // Call the findById() method of the userService
        User foundUser = userService.findById(userId);

        // Verify that the returned user is null
        assertNull(foundUser);
        // Verify that userRepository.findById() was called once with the correct ID
        verify(userRepository, times(1)).findById(userId);
    }
}
