package com.openclassrooms.starterjwt.ut.services;

import com.openclassrooms.starterjwt.exception.BadRequestException;
import com.openclassrooms.starterjwt.exception.NotFoundException;
import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.SessionRepository;
import com.openclassrooms.starterjwt.repository.UserRepository;
import com.openclassrooms.starterjwt.services.SessionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SessionServiceTest {

    @Mock
    private SessionRepository sessionRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private SessionService sessionService;


    @Test
    void testCreateSession() {
        Session session = new Session();
        when(sessionRepository.save(session)).thenReturn(session);

        Session createdSession = sessionService.create(session);

        assertNotNull(createdSession);
        assertEquals(session, createdSession);
        verify(sessionRepository, times(1)).save(session);
    }

    @Test
    void testDeleteSession() {
        Long sessionId = 1L;
        doNothing().when(sessionRepository).deleteById(sessionId);

        assertDoesNotThrow(() -> sessionService.delete(sessionId));
        verify(sessionRepository, times(1)).deleteById(sessionId);
    }

    @Test
    void testFindAllSessions() {
        List<Session> sessions = new ArrayList<>();
        when(sessionRepository.findAll()).thenReturn(sessions);

        List<Session> foundSessions = sessionService.findAll();

        assertNotNull(foundSessions);
        assertEquals(sessions, foundSessions);
        verify(sessionRepository, times(1)).findAll();
    }

    @Test
    void testGetSessionById() {
        Long sessionId = 1L;
        Session session = new Session();
        when(sessionRepository.findById(sessionId)).thenReturn(Optional.of(session));

        Session foundSession = sessionService.getById(sessionId);

        assertNotNull(foundSession);
        assertEquals(session, foundSession);
        verify(sessionRepository, times(1)).findById(sessionId);
    }

    @Test
    void testUpdateSession() {
        Long sessionId = 1L;
        Session session = new Session();
        when(sessionRepository.save(session)).thenReturn(session);

        Session updatedSession = sessionService.update(sessionId, session);

        assertNotNull(updatedSession);
        assertEquals(session, updatedSession);
        verify(sessionRepository, times(1)).save(session);
    }

    @Test
    void testParticipateInSession() {
        Long sessionId = 1L;
        Long userId = 1L;
        Session session = mock(Session.class);
        User user = new User();

        List<User> usersList = new ArrayList<>();
        // Add any necessary setup for the usersList, if needed

        // Mock the behavior of session.getUsers() to return a non-null list
        when(session.getUsers()).thenReturn(usersList);

        when(sessionRepository.findById(sessionId)).thenReturn(Optional.of(session));
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        assertDoesNotThrow(() -> sessionService.participate(sessionId, userId));
        assertTrue(session.getUsers().contains(user));
        verify(sessionRepository, times(1)).save(session);
    }

    @Test
    void testNoLongerParticipateInSession() {
        Long sessionId = 1L;
        Long userId = 1L;
        Session session = mock(Session.class);

        // Mock the behavior of getUsers() to return an empty list
//        when(session.getUsers()).thenReturn(new ArrayList<>());

        when(sessionRepository.findById(sessionId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> sessionService.noLongerParticipate(sessionId, userId));
        verify(sessionRepository, never()).save(any(Session.class));
    }

    @Test
    void testParticipateInSession_NotFound() {
        Long sessionId = 1L;
        Long userId = 1L;

        when(sessionRepository.findById(sessionId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> sessionService.participate(sessionId, userId));
        verify(sessionRepository, never()).save(any(Session.class));
    }

    @Test
    void testNoLongerParticipateInSession_NotFound() {
        Long sessionId = 1L;
        Long userId = 1L;

        when(sessionRepository.findById(sessionId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> sessionService.noLongerParticipate(sessionId, userId));
        verify(sessionRepository, never()).save(any(Session.class));
    }

    @Test
    void testParticipateInSession_AlreadyParticipated() {
        Long sessionId = 1L;
        Long userId = 1L;
        Session session = mock(Session.class);

        // Mock the behavior of getUsers() to return a list with one user
        List<User> usersList = new ArrayList<>();
        usersList.add(new User());
//        when(session.getUsers()).thenReturn(usersList);

        when(sessionRepository.findById(sessionId)).thenReturn(Optional.of(session));

        assertThrows(NotFoundException.class, () -> sessionService.participate(sessionId, userId));
        verify(sessionRepository, never()).save(any(Session.class));
    }

    @Test
    void testParticipateInSession_NotParticipated() {
        Long sessionId = 1L;
        Long userId = 1L;
        Session session = mock(Session.class);

        // Mock the behavior of getUsers() to return an empty list
        when(session.getUsers()).thenReturn(new ArrayList<>());

        when(sessionRepository.findById(sessionId)).thenReturn(Optional.of(session));

        assertThrows(BadRequestException.class, () -> sessionService.noLongerParticipate(sessionId, userId));
        verify(sessionRepository, never()).save(any(Session.class));
    }
}
