package com.openclassrooms.starterjwt.ut.repository;

import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.repository.SessionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class SessionRepositoryTest {

    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    private DataSource dataSource; // Inject DataSource

    @BeforeEach
    public void setUp() {
        // Execute the SQL script before each test
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator(
                new ClassPathResource("schema.sql"));
        populator.execute(dataSource);
    }


    @Test
    public void testFindBySomeCriteria() {
        // Create and save a Session entity
        Session session = new Session();
        session.setName("Test Session");
        session.setDescription("Test Description");

        session.setDate(new Date());

        sessionRepository.save(session);

        // Retrieve sessions from the repository based on some criteria
        List<Session> foundSessions = sessionRepository.findAll();

        // Verify that the retrieved sessions match the expected result
        assertEquals(1, foundSessions.size());
        assertEquals("Test Session", foundSessions.get(0).getName());
        assertEquals("Test Description", foundSessions.get(0).getDescription());
    }
}
