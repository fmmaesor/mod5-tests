package com.openclassrooms.starterjwt.ut.repository;

import com.openclassrooms.starterjwt.models.Teacher;
import com.openclassrooms.starterjwt.repository.TeacherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.TestPropertySource;

import javax.sql.DataSource;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(locations = "classpath:application.test.properties")
public class TeacherRepositoryTest {

    @Autowired
    private DataSource dataSource; // Inject DataSource

    @Autowired
    private TeacherRepository teacherRepository;

    @BeforeEach
    public void setUp() {
        // Execute the SQL script before each test
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator(
                new ClassPathResource("teacher-schema.sql"));
        populator.execute(dataSource);
    }

    @Test
    public void testSaveTeacher() {
        // Create a Teacher entity
        Teacher teacher = new Teacher();
        teacher.setFirstName("John");
        teacher.setLastName("Doe");

        // Save the teacher entity
        teacherRepository.save(teacher);

        // Retrieve the saved teacher
        Teacher savedTeacher = teacherRepository.findById(teacher.getId()).orElse(null);

        // Verify that the retrieved teacher matches the expected result
        assertEquals("John", savedTeacher.getFirstName());
        assertEquals("Doe", savedTeacher.getLastName());
    }
}
