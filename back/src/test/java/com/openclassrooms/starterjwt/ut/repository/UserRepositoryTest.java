package com.openclassrooms.starterjwt.ut.repository;

import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.TestPropertySource;
import javax.sql.DataSource;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@TestPropertySource(locations = "classpath:application.test.properties")
public class UserRepositoryTest {

    @Autowired
    private DataSource dataSource; // Inject DataSource

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        // Execute the SQL script before each test
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator(
                new ClassPathResource("user-schema.sql"));
        populator.execute(dataSource);
    }

    @Test
    public void testExistsByEmail() {
        // Create a user entity
        User user = new User();
        user.setEmail("test@example.com");
        user.setPassword("password");

        // Save the user entity
        userRepository.save(user);

        // Check if the user exists by email
        assertTrue(userRepository.existsByEmail("test@example.com"));
    }
}
