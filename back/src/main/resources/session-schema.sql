CREATE TABLE IF NOT EXISTS SESSIONS (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    date DATE NOT NULL,
    description VARCHAR(2500) NOT NULL,
    teacher_id BIGINT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    FOREIGN KEY (teacher_id) REFERENCES Teacher(id) -- Assuming Teacher is an entity with an 'id' field
);