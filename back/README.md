# Yoga App !

Build the project using Maven:
> mvn clean install

Launch the application:
> java -jar target/application-name.jar

 Replace `application-name` with the actual name of the generated JAR file.


For launch and generate the jacoco code coverage:
> mvn clean test

GL
